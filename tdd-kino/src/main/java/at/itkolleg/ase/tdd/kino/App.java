package at.itkolleg.ase.tdd.kino;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * Dieses Beispiel stammt aus https://training.cherriz.de/cherriz-training/1.0.0/testen/junit5.html
 */
public class App 
{
    public static void main( String[] args )
    {
        //Saal anlegen
        Map<Character,Integer> map = new HashMap<>();
        map.put('A',10);
        map.put('B',10);
        map.put('C',15);
        KinoSaal ks = new KinoSaal("LadyX",map);

        Vorstellung vorstellung = new Vorstellung(ks,Zeitfenster.ABEND, LocalDate.now(),"Test",10);
        Ticket ticket = vorstellung.kaufeTicket('A',10,40);
        System.out.println("Du hast folgendes Ticket gekauft" + ticket.getSaal() +" " + ticket.getPlatz() + " "+ ticket.getReihe());

        KinoVerwaltung kinoVerwaltung = new KinoVerwaltung();
        kinoVerwaltung.einplanenVorstellung(vorstellung);
        kinoVerwaltung.kaufeTicket(vorstellung,'A',1,10);

        //Platz prüfen
        System.out.println(ks.pruefePlatz('A',11));
        System.out.println(ks.pruefePlatz('A',10));
        System.out.println(ks.pruefePlatz('B',10));
        System.out.println(ks.pruefePlatz('C',14));

        //...

    }
}
