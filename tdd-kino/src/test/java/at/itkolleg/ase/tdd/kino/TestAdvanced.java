package at.itkolleg.ase.tdd.kino;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class TestAdvanced {

    private List<Vorstellung> vorstellungen = new ArrayList<>();
    private KinoVerwaltung kinoVerwaltung;
    private KinoSaal ks1;
    private LocalDate date = LocalDate.now();
    private Vorstellung v1;
    private Vorstellung v2;
    private Vorstellung v3;

    private Zeitfenster zeitfenster = Zeitfenster.ABEND;
    @BeforeEach
    void setUp() {
        Map<Character,Integer> map = new HashMap<>();
        map.put('A',10);
        map.put('B',10);
        map.put('C',15);

        ks1 = new KinoSaal("SuperSaal",map);

        v1 = new Vorstellung(ks1, zeitfenster, date, "Fast and Furious", 15.99f);
        v2 = new Vorstellung(ks1, Zeitfenster.NACHMITTAG, date, "Harry Potter", 15.99f);
        v3 = new Vorstellung(ks1, Zeitfenster.NACHT, date, "Herr der Ringe", 15.99f);

        kinoVerwaltung = new KinoVerwaltung();

        kinoVerwaltung.einplanenVorstellung(v1);
    }

    @Test
    void vorstellungAnlegen() {
        Vorstellung vorstellung = new Vorstellung(ks1, zeitfenster, date, "Fast and Furious", 15.99f);
        assertNotNull(vorstellung);
        assertEquals(ks1, vorstellung.getSaal());
        assertEquals(zeitfenster, vorstellung.getZeitfenster());
        assertEquals(date, vorstellung.getDatum());

    }

    @Test
    void testMehrereVorstellungenEinplanen() {
        assertDoesNotThrow(() -> kinoVerwaltung.einplanenVorstellung(v2));
        assertDoesNotThrow(() -> kinoVerwaltung.einplanenVorstellung(v3));
    }

    @Test
    void testDoppelteVorstellungEinplanen() {
        assertThrows(IllegalArgumentException.class, () -> kinoVerwaltung.einplanenVorstellung(v1));
    }

    private static Stream<Arguments> ticketKaufParameter() {
        return Stream.of(
                Arguments.of('A', 1, 25.0f),
                Arguments.of('A', 5, 22.0f),
                Arguments.of('B', 8, 20.0f),
                Arguments.of('B', 4, 20.5f)
        );
    }

    @ParameterizedTest
    @MethodSource("ticketKaufParameter")
    void testKaufeTicketMitVerschiedenenParametern(char reihe, int platz, float geld) {
        Ticket ticket = v1.kaufeTicket(reihe, platz, geld);

        assertNotNull(ticket);
        assertEquals(reihe, ticket.getReihe());
        assertEquals(platz, ticket.getPlatz());
    }

    @TestFactory
    List<DynamicTest> testKaufeTicketTestfactory() {
        int testAnzahl = 5; // Anzahl der zufälligen Tests
        long seed = 30; //
        /*
         * Wird verwendet, um Random mit einem Seed zu erstellen. Dies ermöglicht es, die mit Random generierten
         * Zufallswerte reproduzierbar zu machen. Das ist, vor allem mit Tests, die mehrmals ausgeführt werden, wichtig,
         * da diese konstant sein sollten.
         */
        Random random = new Random(seed);
        List<DynamicTest> dynamicTests = new ArrayList<>(); //Liste mit Tests

        for (int i = 0; i < testAnzahl; i++) {
            char reihe = (char) ('A' + random.nextInt(4)); // Zufällige Reihen: 'A', 'B', 'C', 'D'
            int platz = random.nextInt(19) + 1; // Zufällige Plätze: 1-20
            float geld = random.nextFloat() * 20; // Zufällige Geldbeträge: 0.0 - 20.0

            /*
             * In jedem Schleifendurchlauf wird ein DynamicTest-Objekt erstellt und mit den Testwerten befüllt.
             * Der erste Parameter des DynamicTests ist der DisplayName, der nach Ablauf des Tests angezeigt und mit
             * den verwendeten Werten befüllt ist. Anschließend werden in einer Lambda-Expression die eigentlichen
             * Tests durchgeführt. Im Try-Block wird geprüft, ob das Ticket korrekt erstellt wurde und nicht "null" ist,
             * ob die Reihe der vorgegebenen entspricht und ob der Platz dem vorgegebenen entspricht. Sollten
             * Exception auftreten, werden diese in den catch-Blöcken gefangen und auf ihre Korrektheit geprüft.
             */
            DynamicTest dynamicTest = DynamicTest.dynamicTest(
                    "Ticketkauf wird getestet mit: " + reihe + ", Platz: " + platz + ", Geld: " + geld, () -> {

                        try {
                            Ticket ticket = kinoVerwaltung.kaufeTicket(v1, reihe, platz, geld);
                            assertNotNull(ticket);
                            assertEquals(reihe, ticket.getReihe());
                            assertEquals(platz, ticket.getPlatz());
                        } catch (IllegalArgumentException e) {
                            assertTrue(e.getMessage().equals("Nicht ausreichend Geld.")
                                            || e.getMessage().startsWith("Der Platz "),
                                    "Unerwartete IllegalArgumentException: " + e.getMessage());
                        } catch (IllegalStateException e) {
                            assertTrue(e.getMessage().startsWith("Der Platz "),
                                    "Unerwartete IllegalStateException: " + e.getMessage());
                        }
                    });

            dynamicTests.add(dynamicTest);
        }

        return dynamicTests;
    }

}
