package at.itkolleg.ase.tdd.kino;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;


public class TestKinoVerwaltung {

    KinoVerwaltung verwaltung;
    List<Vorstellung> vorstellungen;
    KinoSaal ks;

    @BeforeEach
    void setup(){
        verwaltung = new KinoVerwaltung();
        vorstellungen = new ArrayList<>();
        Map<Character, Integer> map = new HashMap<>();
        map.put('A', 10);
        map.put('B', 10);
        map.put('C', 15);
        ks = new KinoSaal("KS1",map);
    }

    @Test
    void testGetVorstellungen(){
        Vorstellung v1 = new Vorstellung(ks,Zeitfenster.NACHMITTAG,LocalDate.now(),"Testfilm",20);
        vorstellungen.add(v1);
        assertTrue(vorstellungen.contains(v1));

    }
    @Test
    void testVorstellungEinplanen(){
        Vorstellung v1 = new Vorstellung(ks,Zeitfenster.ABEND,LocalDate.now(),"Testfilm",20);
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            verwaltung.einplanenVorstellung(v1);
            verwaltung.einplanenVorstellung(v1);
        });

    }


}
