package at.itkolleg.ase.tdd.kino;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestVorstellung {
    private Vorstellung vorstellung;
    @Mock
    private Vorstellung vorstellungMock;
    private KinoSaal ks;

    @BeforeEach
    void setup() {
        Map<Character, Integer> map = new HashMap<>();
        map.put('A', 10);
        map.put('B', 10);
        map.put('C', 15);
        ks = new KinoSaal("KS1", map);
        vorstellung = new Vorstellung(ks, Zeitfenster.ABEND, LocalDate.now(), "Testfilm", 20);
        vorstellungMock = Mockito.mock(Vorstellung.class);
    }

    @Test
    void testGetFilm() {
        assertEquals("Testfilm", vorstellung.getFilm());
    }

    @Test
    void testGetFilmMockito() {
        //wenn vom Stub die getFlim aufgerufen wird, soll Testfilm zurückkommen
        Mockito.when(vorstellungMock.getFilm()).thenReturn("Testfilm");
        //Schauen ob der Wert korrekt gemockt wurde
        assertEquals("Testfilm", vorstellungMock.getFilm());
        Mockito.verify(vorstellungMock).getFilm();
    }

    @Test
    void testGetSaal() {
        Map<Character, Integer> map = new HashMap<>();
        map.put('A', 10);
        map.put('B', 10);
        map.put('C', 15);
        assertEquals(new KinoSaal("KS1", map), vorstellung.getSaal());
    }

    @Test
    void testGetZeitfenster() {
        assertEquals(Zeitfenster.ABEND, vorstellung.getZeitfenster());
    }

    @Test
    void testGetZeitfensterMock(){
        Mockito.when(vorstellungMock.getZeitfenster()).thenReturn(Zeitfenster.ABEND);
        //überprüfen ob der Wert korrekt gemockt wurde
        assertEquals(Zeitfenster.ABEND, vorstellungMock.getZeitfenster());
    }

    @Test
    void testGetDatum() {
        assertEquals(LocalDate.now(), vorstellung.getDatum());
    }

    @Test
    void testKaufeTicket() {
        assertTrue(new Ticket("KS1", Zeitfenster.ABEND, LocalDate.now(), 'A', 10).equals(vorstellung.kaufeTicket('A', 10, 20)));
    }

    @Test
    void testKaufeTicketMock() {
        Mockito.when(vorstellungMock.kaufeTicket('A', 10, 20)).thenReturn(new Ticket("KS1", Zeitfenster.ABEND, LocalDate.now(), 'A', 10));
        assertTrue(new Ticket("KS1", Zeitfenster.ABEND, LocalDate.now(), 'A', 10).equals(vorstellung.kaufeTicket('A', 10, 20)));

        Mockito.when(vorstellungMock.kaufeTicket('A', 10, 5)).thenThrow(new IllegalArgumentException("Nicht ausreichend Geld."));
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            vorstellungMock.kaufeTicket('A', 10, 5);
        });
    }
}
