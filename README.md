# TDD Test Driven Development
Beim TDD wird zuerst der Test geschrieben. Diese schlägt dann natürlich fehl. 
EIn Feature wird so lange entwickelt, bis der Test erfolgreich ist. Anschließend kann mit dem >Refactoring begonnen werden.
Durch den test kann sichergestellt werden, dass der Code auch nach dem Refactoring noch funktioniert. TDD kommt aus dem extreme programming und wurde von Kent Beck entwickelt.
Den Ausgangspunkt von TDD stellen in kleine Einheiten aufgeteilte Anforderungen, auch Microfeatures dar.

## Microfeatures
Ausgehend vom ganzen System werden zunächst Epics abgeleitet, deren Umsetzung mehere Monate benötigen kann.
Von den Epics werden dann Features abgeleitet, deren Umsetzung mehrere Wochen in Anspruch nehmen kann.
Dann werden die Stories abgeleitet, die mehrere Tage dauern. Diese Aufgaben führt der Product Owner durch.
Von den Stories leitet der Entwickler einzelne Tasks, die innerhalb von Stunden erledigt werden können ab.
Die unterste Ebene bilden die Microfeatures, die in wenigen Minuten abgearbeitet werden können.
![img.png](microfeatures.png)

## Microiterations
Die Microfeatures werden in Microiterations entwickelt. Eine normale Iteration ist z.B. in Scrum ein Sprint.
Während eines Sprints werden von einem Entwickler die UserStories bearbeitet.
Diese werden in einzelne Tasks und Microfeatures unterteilt.
Jedes Microfeature wird in einer Microiteration immer nach der gleichen Vorgehensweise entwickelt.
Zunächst wird der Test entwickelt. Dieser schlägt natürlich fehl, da das Feature noch nicht implementiert wurde.
Da fehlgeschlagene Tests in der IDE in Rot gekennzeichnet werden, ist das die Phase Red.
Nun wird das Feature implementiert, bis der Test grün ist. Dieser Status wird als Phase Green bezeichnet.
Anschließend wird das Refactoring durchgeführt, um die Codequalität zu verbessern.
Durch die Tests kann sofort erkannt werden wenn der Code nicht mehr funktioniert.
Es werden auch die Tests der vorhergehenden Features ausgeführt. Daher sieht man sofort wenn
etwas durch das Refactoring nicht mehr funktioniert.
![img.png](microinterations.png)

## Vorteile von TDD
* qualitative hochwertige Software durch regelmäßiges Testen
* Stabilität : die Funktionalität der Software ist durch Tests sichergestellt
* Seiteneffekte durch Reforcoting auf den restlichen Code können ausgeschlossen werden
* Klassen werden durch die Notwendigkeit des Testens testbar strukturiert

## Nachteile von TDD
* teuere Entwicklung durch das Schreiben von Tests
*  langsamere Entwicklung durch das Schreiben von Tests
* Tests müssen bei einer Änderung auch angepasst werden

## Arten von Tests
Es gibt verschiedene Arten von Tests.
### Unit Test
Beim Unit Test wird eine einzelne Unit getestet. Eine Unit ist ein einzelner Baustein im Code.
Meist wird eine Funktion als Unit betrachtet. Eine Funktion lässt sich gut testen, wenn sich von anderen Funktionen isoliert ist.
Die Unit Tests machen den größten Teil der Tests aus und mit ihnen kann ein großer Teil der Codebasis getestet werden.
Für Tests in Java wird beispielsweise JUnit verwendet.
### Integration Test
Der Integration Test testet die Zusammenarbeit einzelner Komponenten.
Dieser wird zum Beispiel verwendet, wenn eine Klasse auf das Netzwerk zugreift.
Diese Art von Tests ist deutlich aufwändiger als ein Unit Test.
### UI Tests
Beim UI Test wird die Funktionalität einer App z.B. im Browser getestet.
Diese Tests sind langsam da sie auf verschiedenen Browser und auch auf verschiedenen Betriebssystem durchgeführt werden müssen.
Die Tests werden zum Beispiel mit Selenium durchgeführt.
### Akzeptanztests
Bei Akzeptanztests wird die Benutzerfreundlichkeit der Funktionen überprüft.
Dazu verwenden Benutzer die Software und gegen Feedback zu den Funktionen.

## Testpyramide
Die Testpyramide sagt aus, wie viel von welchen Testarten durchgeführt werden sollten.
An unterster Stelle stehen die Unit Tests. Diese sind schnell und in der Entwicklung einfach.
An 2. Stelle stehen die Integrationstests. Diese sind schon aufwändiger als die Unit Tests,
da auf Abhängigkeiten von außen wie z.B. eine Datenbank zugegriffen werden muss.
An oberster Stelle stehen die Akzeptantstests da Ausführung diese sehr aufwändig ist.
Es gibt auch einen Ansatz der die Testpyramide umdreht, da die Akzeptanzstests am aussagekräftigsten sind.
![img.png](testpyramide_umgekehrt.png)

## Mockito
Mockito wird verwendet, um Mock Objekte zu erzeugen. Mock Objekte werden für das Testen anstellen von echten Objekten als Platzhalter verwendet.
Somit können Klassen isoliert von ihrer Umgebung getrennt getestet werden.
Dadurch kann zum Beispiel in einem Spring Boot Projekt der Service Layer getrennt vom Datenbanklayer getestet werden.
Der Service Layer bekommt die Daten von dem Mock Objekten anstelle von der Datenbank.

## TDD is Dead?
Um Code automatisiert testen zu können, muss dieser mit möglichst wenigen Abhängigkeiten geschrieben werden. 
Das Gegenteil zum automatisierten Testen wäre das manuelle testen, beidem der Code ausgeführt wird und man überprüft, ob
dieser das macht, was man zuvor geplant hat. Dies wird jedoch durch automatisiertes Testen erleichert. 
Durch den Einsatz von TDD muss man den Code so schreiben, dass dieser testbar ist. Dadurch muss die Kopplung reduziert werden.
Somit wird auch die Wartbarkeit verbessert und somit entsteht qualitativ hochwertiger Code.
Durch die Verwendung von TDD muss man sich schon zu beginn überlegen was genau die Funktion tut und was diese zurückliefert.
Jedoch sollte TDD nicht verwendet werden, wenn man nocht nicht genau weiß wie man ein Problem lösen kann. 
Denn bei jedem Refactoring müssen die Tests auch mitangepasst werden. Wenn man zum Beispiel etwas an einer Klasse ändert
dann müssen auch die Tests angepasst werden. Dadurch erhöht sich der Entwicklungsaufwand. Hier ist es besser sich zuerst
einen Überblick über das zu lösende Problem zu verschaffen und zu versuchen dieses mit Code zu lösen. Dieser Code wird 
jedoch nicht im Produktivsystem eingesetzt werden sondern dient nur zum Ausprobieren. Wenn man die Anforderung verstanden hat
wird dieser Code verworfen und man beginnt die dem Schreiben der Tests und anschließend mit der Implementierung.
Wenn man bereits mehr Erfahrung im Schreiben von Code mit geringer Kopplung und hoher Kohäsion hat, kann man auch zuerst den Code und 
dann den Test schreiben. Dadurch müssen bei Veränderungen im Code nicht die Tests angepasst werden. Dieser werden erst geschrieben,
wenn klar ist, wie das Problem zu lösen ist.